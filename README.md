# shqu - Simple Help Query Utility
Version 0.01 *2021/08/25*

```
$ shqu [options]
```

# Description
**shqu** a help utility for the graphical environment. When the user invokes **shqu** it will collect all **man(1)** and **info(1)** pages as well as all configuration files in */usr/share/examples* and all documentation files in */usr/src/hardware/doc* and */usr/src/os/doc* and then prompt the user to select help item they would like to view using **PROMPT_MENU**. If the returned string from **PROMPT_MENU** is a **man(1)** page (i.e. has the syntax *page (number)*), then **shqu** will open it within a **TERMINAL**. If the returned string from **PROMPT_MENU** is a **info(1)** page (i.e. has the syntax *"(page...)..."*), then **shqu** will open it within a **TERMINAL**. If the returned string is a file on the operating system, then **shqu** will open it in a **TERMINAL** and display the file using **PAGER**. If the returned string does not match or meet any of the prior requirements, then it will be opened in a **BROWSER**. Any arguments provided to **shqu** will be prepended onto **PROMPT_MENU** before any *'-'* arguments.

# ENVIRONMENT
* **PROMPT_MENU**

A program that accepts newline lists from stdin and can take a prompt string (example: dmenu(1)).

* **TERMINAL**

A terminal emulator.

* **BROWSER**

A graphical web browser.

* **PAGER**

A text pager.

* **SHELL**

A POSIX-compliant shell.

# EXAMPLES
* Run **shqu** without passing any extra arguments to **PROMPT_MENU**.
```
$ shqu
```
* Run **shqu** and pass extra arguments to **PROMPT_MENU**.
```
$ shqu -foo -bar
```
* Run **shqu** and pass extra arguments and their parameters to **PROMPT_MENU**.
```
$ shqu -foo "bar" -buzz "bax"
```

# EXIT STATUS
* **0**

Successfully completed the specified task.

* **>0**

An error occurred.

.SH SEE ALSO
* man(1)
* info(1)
* hier(1)
* dmenu(1)
* rofi(1)
* st(1)
* xterm(1)
* firefox(1)
* chromium(1)
* nvim(1)
* less
* sh(1)
